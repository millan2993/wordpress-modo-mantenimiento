<?php
/*
Plugin Name: Modo Mantenimiento
Plugin URI: https://gitlab.com/millan2993/wordpress-modo-mantenimiento
Description: Plugin muy sencillo para colocar una página en modo mantenimiento.
Version: 1.0
Author: Alex Millán <CosechaFresk>
Author URI: http://cosechafresk.com
License: MIT
*/

function wp_maintenance_mode() {

    if (!current_user_can('edit_themes') || !is_user_logged_in() ) {
        wp_die(
        	"<h1>Gracias por visitarnos</h1> <br/>
        	Estamos implementando algunas mejoras, por favor regresa en 30 minutos. <br>
        	Te esperamos !!");
    }
}
add_action('get_header', 'wp_maintenance_mode');